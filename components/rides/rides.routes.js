function ridesComponentRoutes() {
    const ctrl = require('./rides.controller');

    return (open, closed) => {
        open.route('/rides').post(ctrl.addRides);
        open.route('/rides').get(ctrl.getRides);
    }
}

module.exports = ridesComponentRoutes()