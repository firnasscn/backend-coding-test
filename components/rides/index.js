module.exports = Object.freeze({
    model: require('./rides.model'),
    ctrl: require('./rides.controller'),
    routes: require('./rides.routes')
  })