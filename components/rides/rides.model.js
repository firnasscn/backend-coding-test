const mongoose = require('mongoose');

const RidesSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    route: {
        type: String
    },
    status: {
        type: Number,
        default: 1
    }
}, { timestamps: true });

module.exports = mongoose.model('rides', RidesSchema);