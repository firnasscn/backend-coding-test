const Response = require('../../utils/response');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Rides = require('./rides.model')


function ridesComponentCtrl() {
    const methods = {
        addRides: async(req, res) => {
            try {
                let { name, route } = req.body;

                let isAlreadyExist = await Rides.find({
                    name,
                    status: 1
                });   

                if(isAlreadyExist.length > 0 ) {
                    return Response.error(res, 400, "Ride already exsist")
                };

                let result = await Rides.create({
                    name, route
                });

                return Response.success(res, result, 'rides added successfully.');
            } catch (e) {
                return Response.errorInternal(e, res)
            }
        },
        getRides: async(req, res) => {
            try {
                let gIntDataPerPage = 10;
                let page = req.query.page || 1;
                let skipRec = page - 1;
                skipRec = skipRec * gIntDataPerPage;

                let count = await Rides.countDocuments({ status: 1 })

                let rides = await Rides.find({
                    status: 1
                }).skip(skipRec).limit(gIntDataPerPage);   

                let lObjRides = {
                    items: rides,
                    total: Math.ceil(count / gIntDataPerPage),
                    totalProjects: count,
                    per_page: gIntDataPerPage,
                    currentPage: page
                }
                
                return Response.success(res, lObjRides, 'Fetch Ride.');
            } catch (e) {
                console.log(e)
                return Response.errorInternal(e, res)
            }
        }
    }
    return Object.freeze(methods)
}

module.exports = ridesComponentCtrl()