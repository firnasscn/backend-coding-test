'use strict'
require('dotenv').config();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const helmet = require('helmet');
const glob = require('glob');
const cors = require('cors')
const path = require('path')
const fs = require('fs')
require('./config/db')

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./docs/swagger.json');

const opts = {
    explorer: false,
    swaggerOptions: {
        validatorUrl: null
    },
    customSiteTitle: 'Backend REST Service'
};

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, opts));
app.get('/', (req, res) => {
    res.redirect('/api-docs');
});

app.enable('trust proxy')
app.use(helmet())

app.use(
    bodyParser.json({
        limit: '50mb'
    })
)
app.use(
    bodyParser.urlencoded({
        limit: '50mb',
        extended: true
    })
);

app.use(morgan('dev'));

morgan.token('body', function getId(req, res) {
    return JSON.stringify(req.body)
})
morgan.token('json', function getId(req, res) {
    return JSON.stringify(res.__morgan_body_response)
})

let accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })

// log all requests to access.log
app.use(morgan(':method :url :body :status :json :response-time ms ', {
    stream: accessLogStream
}));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*') /* *.name.domain for production  */
    res.setHeader('Access-Control-Allow-Headers', '*')
    next()
})
app.use(cors());

const openRouter = express.Router() // Open routes
const apiRouter = express.Router() // Protected routes

glob('./components/*', null, (err, items) => {
    items.forEach(component => {
        require(component).routes && require(component).routes(
            openRouter,
            apiRouter
        )
    })
});

/** We are not using authentication */
// apiRouter.use(authenticate.verifyToken)

app.use('/v1', openRouter)
app.use('/api/v1', apiRouter);

module.exports = app
