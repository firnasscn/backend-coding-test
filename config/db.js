const mongoose = require('mongoose');
const { logger } = require("../logger");

function connectDb() {
    mongoose.connect(process.env.DBURI, { useNewUrlParser: true, useUnifiedTopology: true } )
    mongoose.connection.on('connected', function() {
        console.log('Mongoose default connection is open to ', process.env.DBURI)
        logger.info('Mongoose default connection is open to ', process.env.DBURI)
    })

    mongoose.connection.on('error', function(err) {
        logger.debug('Mongoose default connection has occured ' + err + ' error')
    })

    mongoose.connection.on('disconnected', function(err) {
        logger.info('Mongoose default connection is disconnected')
    })

    process.on('SIGINT', function() {
        mongoose.connection.close(function() {
            logger.info('Mongoose default connection is disconnected due to application termination')
        })
    })
}

module.exports = connectDb()