const app = require("./app");
const Response = require("./utils/response");
const { logger } = require("./logger");

const PORT = process.env.PORT || 8010;

const server = app.listen(PORT, () => {
  console.log(`Listening to port ${PORT}`);
});

app.get("/health", (req, res) => {
  logger.info("Health check")
  return Response.success(res, "", "Health check");
});

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.debug("Server closed");
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (err) => {
  console.log(err)
  exitHandler();
};

process.on("uncaughtException", unexpectedErrorHandler);
process.on("unhandledRejection", unexpectedErrorHandler);

process.on("SIGTERM", () => {
  if (server) {
    server.close();
  }
});
