const { createLogger, format, transports } = require("winston");

const { combine, timestamp, printf, prettyPrint } = format;

const customFormat = printf(
  ({ level, message, timestamp }) => `${timestamp} ${level}: ${message}`
);

const logger = createLogger({
  level: "info",
  format: combine(timestamp(), customFormat),
  transports: [new transports.File({ filename: "log/backend-test.log" })],
});

if (process.env.NODE_ENV !== "development") {
  logger.add(
    new transports.Console({
      level: "debug",
      format: combine(timestamp(), prettyPrint()),
    })
  );
}

module.exports = {
  logger
};
